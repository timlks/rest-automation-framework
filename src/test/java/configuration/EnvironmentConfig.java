package configuration;

import java.io.File;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class EnvironmentConfig extends ConfigurationManager {
	
	private Config config;
	private String baseUri;
	private String basePath;
	private String username;
	private String password;
	private File configFile;
	
	public EnvironmentConfig() {
		this.configFile = getConifgFile();
		loadConfigFile();
		loadConfiguration();
	}
	
	public void loadConfigFile() {
		config = ConfigFactory.parseFile(configFile).getConfig("vdiLocal").resolve();
	}
	
	@Override
	public void loadConfiguration() {
		baseUri = config.getString("base-uri");
		basePath = config.getString("base-path");
		username = config.getString("username");
		password = config.getString("password");
	}
	
	/**
	 * @return the base uri
	 */
	public String getBaseUrl() {
		return baseUri;
	}
	
	/**
	 * @return the base path
	 */
	public String getBasePath() {
		return basePath;
	}
	
	public String getUsername() {
		return username;
	}
	
	public String getPassword() {
		return password;
	}
	
}
