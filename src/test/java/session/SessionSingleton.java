package session;

import java.net.MalformedURLException;

public class SessionSingleton {
	private static Session sessionInstance;
	
	private SessionSingleton() {
	}
	
	/**
	 * Returns the current SessionCCSUI object associated with the test context.
	 * If the session doesn't currently existing a new session is created.
	 * 
	 * @return Session object configured with the details in the "environment.conf"
	 *         file
	 * 
	 * @throws MalformedURLException
	 *             if the "hub-url" property is not a valid URL
	 */
	public synchronized static Session getSession() throws MalformedURLException {
		if (sessionInstance == null) {
			sessionInstance = new Session();
		}
		return sessionInstance;
	}
}
