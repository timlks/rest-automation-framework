package session;

import org.springframework.stereotype.Component;

import configuration.EnvironmentConfig;

/**
 * 
 * @author Timothy Lks
 *
 */
@Component
public class Session {
	
	private EnvironmentConfig configuration;
	
	public Session() {
		configuration = new EnvironmentConfig();
	}
	
	/**
	 * @return the BASE URL
	 */
	public String getBaseUrl() {
		return configuration.getBaseUrl();
	}
	
	/**
	 * @return the BASE PATH
	 */
	public String getBasePath() {
		return configuration.getBasePath();
	}

	public String getUsername() {
		return configuration.getUsername();
	}

	public String getPassword() {
		return configuration.getPassword();
	}
	
}
