package tests;

import static io.restassured.RestAssured.given;
import static helpers.ErrorHelper.*;

import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

import api.RetrievalAPIObject;
import dataobjetcs.ErrorResponse;
import dataobjetcs.Person;
import io.restassured.http.ContentType;

/**
 * 
 * @author TLOUKAS
 *
 *         Contains tests to check the retrieval of people
 */
public class RetrievalTest extends TestBase {

	/**
	 * Scenario: Successful retrieval of person's details
	 * Given the user exists
	 * And provides a valid id
	 * When a GET request is made
	 * Then a 200 response code is returned
	 * And the person's details are returned
	 * 
	 * @throws JsonProcessingException
	 */
	@Test
	public void get_200_with_valid_id() throws JsonProcessingException {

		Person expectedPerson = new Person("1", "John", "Doe", 20, "test@test.com", "Tester");

		//@formatter:off
		Person actualPerson = 
			given()
				.spec(requestSpec)
				.contentType(ContentType.JSON)
			.when()
				.get(RetrievalAPIObject.retrievalURL(1))
			.then()
				.assertThat().statusCode(200)
				.extract().body().as(Person.class);
		//@formatter:on

		SoftAssertions softly = new SoftAssertions();
		softly.assertThat(actualPerson.getId()).isEqualTo(expectedPerson.getId());
		softly.assertThat(actualPerson.getFirstName()).isEqualTo(expectedPerson.getFirstName());
		softly.assertThat(actualPerson.getSurname()).isEqualTo(expectedPerson.getSurname());
		softly.assertThat(actualPerson.getAge()).isEqualTo(expectedPerson.getAge());
		softly.assertThat(actualPerson.getEmail()).isEqualTo(expectedPerson.getEmail());
		softly.assertThat(actualPerson.getOccupation()).isEqualTo(expectedPerson.getOccupation());
		softly.assertAll();
	}

	/**
	 * Scenario: Unsuccessful retrieval of person's details using invalid id
	 * Given the user exists
	 * And provides an invalid id
	 * When a GET request is made
	 * Then a 404 response code is returned
	 * And the appropriate error response is returned
	 * 
	 * @throws JsonProcessingException
	 */
	@Test
	public void get_404_with_invalid_id() throws JsonProcessingException {

		int expectedErrorCode = 404;

		//@formatter:off
		ErrorResponse errorResponse = 
			given()
				.spec(requestSpec)
				.contentType("application/json")
			.when()
				.get(RetrievalAPIObject.retrievalURL(10))
			.then()
				.assertThat().statusCode(expectedErrorCode)
				.extract().body().as(ErrorResponse.class);
		//@formatter:on

		SoftAssertions softly = new SoftAssertions();
		softly.assertThat(errorResponse.getErrorCode()).isEqualTo(expectedErrorCode);
		softly.assertThat(errorResponse.getErrorMessage()).isEqualTo(RESOURCE_NOT_FOUND_ERROR_MESSAGE);
		softly.assertThat(errorResponse.getErrorDescription()).isEqualTo(PERSON_NOT_FOUND_ERROR_DESCRIPTION);
		softly.assertAll();
	}
}
