package tests;

import static org.junit.Assert.fail;
import static io.restassured.RestAssured.config;
import static io.restassured.config.EncoderConfig.encoderConfig;

import java.net.MalformedURLException;
import java.util.Base64;

import org.junit.BeforeClass;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import session.Session;
import session.SessionSingleton;

public class TestBase {

	@Autowired
	protected static Session session;
	protected static RequestSpecification requestSpec;
	private static String authHeader;
	protected static ObjectMapper mapper;

	public static void StepsInit() {
		try {
			session = SessionSingleton.getSession();
		} catch (MalformedURLException e) {
			e.printStackTrace();
			fail("Unable to initialise session: " + e.getMessage());
		}
	}

	@BeforeClass
	public static void initialise() {

		StepsInit();
		String credentials = session.getUsername() + ":" + session.getPassword();
		authHeader = String.format("Basic %s", Base64.getEncoder().encodeToString(credentials.getBytes()));

		mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Include.NON_NULL);

		requestSpec = createDefaultRequestSpec();
	}

	/**
	 * Builds the request specification
	 * 
	 * @return the request specification
	 */
	private static RequestSpecification createDefaultRequestSpec() {
		RestAssured.config = config()
				.encoderConfig(encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false));

		return new RequestSpecBuilder().setBaseUri(session.getBaseUrl()).setBasePath(session.getBasePath())
				.addHeader("Authorization", authHeader).build();
	}
}
