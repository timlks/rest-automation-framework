package tests;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static helpers.ErrorHelper.*;

import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

import api.IngestionAPIObject;
import dataobjetcs.ErrorResponse;
import dataobjetcs.Person;
import io.restassured.http.ContentType;

/**
 * 
 * @author TLOUKAS
 *
 *         Contains tests to check the ingestion of people
 */
public class IngestionTest extends TestBase {

	/**
	 * Scenario: Successful ingestion of person's details
	 * Given the user has provided valid details
	 * When a POST request is made
	 * Then a 201 response code is returned
	 * And the person's details are returned
	 * 
	 * @throws JsonProcessingException
	 */
	@Test
	public void get_201_with_valid_details() throws JsonProcessingException {

		Person expectedPerson = new Person("John", "Smith", 25, "test2@test.com", "Doctor");
		String expectedId = "2";
		
		//@formatter:off
		Person actualPerson = 
			given()
				.spec(requestSpec)
				.contentType(ContentType.JSON)
				.header("Required-Header", "header")
				.body(mapper.writeValueAsString(expectedPerson))
			.when()
				.post(IngestionAPIObject.ingestionURL())
			.then()
				.assertThat().statusCode(201)
				.extract().body().as(Person.class);
		//@formatter:on

		assertThat(actualPerson.getId()).isEqualTo(expectedId);
		assertThat(actualPerson.getFirstName()).isEqualTo(expectedPerson.getFirstName());
	}

	/**
	 * Scenario: Unsuccessful ingestion of person's details using existing email
	 * Given the user has provided invalid details with an existing email
	 * When a POST request is made
	 * Then a 400 response code is returned
	 * And the appropriate error response is returned
	 * 
	 * @throws JsonProcessingException
	 */
	@Test
	public void get_400_with_existing_email() throws JsonProcessingException {

		int expectedErrorCode = 400;
		Person expectedPerson = new Person("John", "Cane", 25, "test@test.com", "Teacher");
		
		//@formatter:off
		ErrorResponse errorResponse = 
			given()
				.spec(requestSpec)
				.contentType("application/json")
				.body(mapper.writeValueAsString(expectedPerson))
				.header("Required-Header", "header")
			.when()
				.post(IngestionAPIObject.ingestionURL())
			.then()
				.assertThat().statusCode(expectedErrorCode)
				.extract().body().as(ErrorResponse.class);
		//@formatter:on

		SoftAssertions softly = new SoftAssertions();
		softly.assertThat(errorResponse.getErrorCode()).isEqualTo(expectedErrorCode);
		softly.assertThat(errorResponse.getErrorMessage()).isEqualTo(BAD_REQUEST_ERROR_MESSAGE);
		softly.assertThat(errorResponse.getErrorDescription()).isEqualTo(EMAIL_ALREADY_EXISTS_ERROR_DESCRIPTION);
		softly.assertAll();
	}

}
