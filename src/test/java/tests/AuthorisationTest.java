package tests;

import static io.restassured.RestAssured.given;
import static helpers.ErrorHelper.*;

import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

import api.RetrievalAPIObject;
import authorization.AuthorizationTestBase;
import dataobjetcs.ErrorResponse;

public class AuthorisationTest extends AuthorizationTestBase {

	/**
	 * Scenario: Unsuccessful retrieval of person's details using invalid
	 * credentials
	 * Given the user exists
	 * And provides a valid id
	 * When a GET request is made using invalid credentials
	 * Then a 401 response code is returned
	 * And the appropriate error response is returned
	 * 
	 * @throws JsonProcessingException
	 */
	@Test
	public void get_401_with_invalid_credentials() throws JsonProcessingException {

		int expectedErrorCode = 401;

		//@formatter:off
		ErrorResponse errorResponse = 
			given()
				.spec(requestSpec)
				.contentType("application/json")
				.auth().preemptive().basic("invalid", "invalid")
			.when()
				.get(RetrievalAPIObject.retrievalURL(2))
			.then()
				.assertThat().statusCode(expectedErrorCode)
				.extract().body().as(ErrorResponse.class);
		//@formatter:on

		SoftAssertions softly = new SoftAssertions();
		softly.assertThat(errorResponse.getErrorCode()).isEqualTo(expectedErrorCode);
		softly.assertThat(errorResponse.getErrorMessage()).isEqualTo(INVALID_AUTHORIZATION_ERROR_MESSAGE);
		softly.assertThat(errorResponse.getErrorDescription()).isEqualTo(INVALID_CREDENTIALS_ERROR_DESCRIPTION);
		softly.assertAll();
	}

	/**
	 * Scenario: Unsuccessful retrieval of person's details using no credentials
	 * Given the user exists
	 * And provides a valid id
	 * When a GET request is made using no credentials
	 * Then a 401 response code is returned
	 * And the appropriate error response is returned
	 * 
	 * @throws JsonProcessingException
	 */
	@Test
	public void get_401_with_no_credentials() throws JsonProcessingException {

		int expectedErrorCode = 401;

		//@formatter:off
		ErrorResponse errorResponse = 
			given()
				.spec(requestSpec)
				.contentType("application/json")
			.when()
				.get(RetrievalAPIObject.retrievalURL(3))
			.then()
				.assertThat().statusCode(expectedErrorCode)
				.extract().body().as(ErrorResponse.class);
		//@formatter:on

		SoftAssertions softly = new SoftAssertions();
		softly.assertThat(errorResponse.getErrorCode()).isEqualTo(expectedErrorCode);
		softly.assertThat(errorResponse.getErrorMessage()).isEqualTo(INVALID_AUTHORIZATION_ERROR_MESSAGE);
		softly.assertThat(errorResponse.getErrorDescription()).isEqualTo(NO_CREDENTIALS_ERROR_DESCRIPTION);
		softly.assertAll();
	}

}
