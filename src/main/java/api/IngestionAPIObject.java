package api;

/**
 * 
 * @author TLOUKAS
 *
 *         Contains methods for ingestion
 */
public class IngestionAPIObject {

	/**
	 * Provides the url for ingestiion
	 * 
	 * @return the URL for ingestion
	 */
	public static String ingestionURL() {
		return "/people";
	}
}
