package api;

/**
 * 
 * @author TLOUKAS
 *
 *         Contains methods for retrieval
 */
public class RetrievalAPIObject {
	
	/**
	 * Provides the url for retrieval
	 * 
	 * @return the URL for retrieval
	 */
	public static String retrievalURL(int id) {
		return String.format("/people/%s", id);
	}
}
