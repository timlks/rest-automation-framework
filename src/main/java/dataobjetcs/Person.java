package dataobjetcs;

//{
//"firstName": "John",
//"surname": "Smith",
//"age": 25,
//"email": "test2@test.com",
//"occupation": "Doctor"
//}
/**
 * 
 * @author TLOUKAS
 *
 *         POJO Class
 */
public class Person {

	private String id;
	private String firstName;
	private String surname;
	private int age;
	private String email;
	private String occupation;

	public Person() {
		super();
	}

	public Person(String id, String firstName, String surname, int age, String email, String occupation) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.surname = surname;
		this.age = age;
		this.email = email;
		this.occupation = occupation;
	}

	public Person(String firstName, String surname, int age, String email, String occupation) {
		super();
		this.firstName = firstName;
		this.surname = surname;
		this.age = age;
		this.email = email;
		this.occupation = occupation;
	}

	public String getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

}
