package helpers;

public class ErrorHelper {

	// Error Messages
	
	public static String BAD_REQUEST_ERROR_MESSAGE = "Bad request received";
	public static String RESOURCE_NOT_FOUND_ERROR_MESSAGE = "Resource not found";
	public static String INVALID_AUTHORIZATION_ERROR_MESSAGE = "Authentication failed due to bad credentials";
	
	// Error Descriptions
	
	public static String EMAIL_ALREADY_EXISTS_ERROR_DESCRIPTION = "That email is already used by another user";
	public static String PERSON_NOT_FOUND_ERROR_DESCRIPTION = "The person you are looking for does not exist";
	public static String INVALID_CREDENTIALS_ERROR_DESCRIPTION = "The username or password was invalid";
	public static String NO_CREDENTIALS_ERROR_DESCRIPTION = "No credentials were provided";
}
