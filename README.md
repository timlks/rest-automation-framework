# Rest Automation Framework
#
# This project was created to show my knowledge on creating and using a testing framework. 
# I write write and run my tests using rest-assured and junit. In addition, i run al my tests
# against wiremock.
# 
# Prerequisites:
# 1. Run the wiremock standalone server:
# The jar can found in src/test/resources/wiremockserver
# To start it, you must run the command:
# java -jar wiremock-standalone-2.24.1.jar --port 8080
# 2. Download and install Eclipse (preferably, while i used that IDE to create the project)
#
# How to run the tests:
# From the command line, go to the project directory and run the command: mvn test
#
# Configuration:
# Config option can be configured to either use the default one, or choose a user created one. 
# To use a user created one, you will need to create an environmental variable called "testConfig" and a file called "environments.conf"
# The value of the variable will be the path to the config file. eg "../folder/environments.conf"